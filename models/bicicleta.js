var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var biciSchema = new Schema({
    code: Number, //no se pone Id porque es una palabra reservada en mongoose
    color: String,
    modelo: String,
    ubicacion: {
        type: [Number],
        index: {type: '2dsphere', sparse: true} //para buscar
    }
});

biciSchema.statics.createInstance = function(code, color, modelo, ubicacion) {
    return new this({
        code: code,
        color: color,
        modelo: modelo,
        ubicacion: ubicacion
    });
};

biciSchema.methods.toString = function() {
    return 'code: ' + this.code + ' | color: ' + this.color;
};

biciSchema.statics.todas = function(callback) {
    return this.find({}, callback); //en las llaves podría ir algo si quiero filtrar
};

biciSchema.statics.add = function(bici, callback){
    this.create(bici, callback);
};

biciSchema.statics.findByCode = function(aCode, callback){
    return this.findOne({code: aCode}, callback);
};

biciSchema.statics.removeByCode = function(aCode, callback){
    return this.deleteOne({code: aCode}, callback);
};

biciSchema.statics.updateByCode = function(aCode, update, callback){
    return this.updateOne({code: aCode}, {$set: update}, callback);
};

module.exports = mongoose.model('Bicicleta', biciSchema);

/* TRABAJANDO CON MODELO SIN PERSISTENCIA

var bici = function(id, color, modelo, ubicacion){
    this.id = id;
    this.color = color;
    this.modelo = modelo;
    this.ubicacion = ubicacion;
}

//defino un método para devolver la información como string
bici.prototype.toString = function(){
    return 'id: ' + this.id + ', color: ' + this.color;
}

//defino un método para ver todas las bicis
bici.todas = [];

//defino un método para agregar una bici al arreglo que contiene todas
bici.add = function(unaBici){
    bici.todas.push(unaBici);
}

//defino un método para buscar una bici en particular
bici.findById = function(biciId){
    var unaBici = bici.todas.find(x => x.id == biciId);
    if (unaBici) { 
        return unaBici
    } else { throw new Error(`No existe una bicicleta con el id ${biciId}`); }
}

//defino un método para remover una bici en particular
bici.removeById = function(biciId){
    //var unaBici = bici.todas.findById(biciId); podría buscarlo solamente para asegurarme que exista
    for (let i = 0; i < bici.todas.length; i++) {
        if (bici.todas[i].id == biciId) {
            bici.todas.splice(i, 1);
            break;            
        }
    }
}


//var bici1 = new bici(1,'verde','montaña', [47.841830,11.1444880]);
//var bici2 = new bici(2,'rojo','deportiva', [47.8471771,11.1510966]);
//var bici3 = new bici(3,'azul','urbana', [47.8383651,11.150069]);
//var bici4 = new bici(4,'negro','deportiva', [47.843365,11.149030]);
//var bici5 = new bici(5,'negro','deportiva', [47.837918,11.144075]);

//bici.add(bici1);
//bici.add(bici2);
//bici.add(bici3);
//bici.add(bici4);
//bici.add(bici5);

module.exports = bici; */