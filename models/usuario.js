var mongoose = require('mongoose');
var Reserva = require('./reserva');
const bcrypt = require('bcrypt'); //módulo que permite encriptar string
const uniqueValidator = require('mongoose-unique-validator');
const crypto = require('crypto');
const Token = require('../models/token');
const mailer = require('../mailer/mailer');
var Schema = mongoose.Schema;

const saltRounds = 10; //inserta cierta aleatoriedad en la encriptación del password

const validateEmail = function(email){
    const re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/; //regex -- expresiones regulares
    return re.test(email);
};

var usuarioSchema = new Schema({
    nombre: {
        type: String,
        trim: true,
        required: [true, 'El nombre es obligatorio.']
    },
    email: {
        type: String,
        trim: true,
        required: [true, 'El email es obligatorio.'],
        unique: true,
        lowercase: true,
        validate: [validateEmail, 'Por favor ingrese un email válido.'],
        match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/]
    },
    password: {
        type: String,
        required: [true, 'El password es obligatorio.']
    },
    passwordResetToken: String,
    passwordResetTokenExpires: Date,
    verificado: {
        type: Boolean,
        default: false
    },
    googleId: String,
    facebookId: String
});

//extensión de mongoose para validar que el email sea único
usuarioSchema.plugin(uniqueValidator, { message: `El {PATH} ya existe con otro usuario`});

//Guarda la password encriptada -- objeto.save (ejecuta la sentencia antes de hacer el save)
usuarioSchema.pre('save', function(next){
    if (this.isModified('password')){
        this.password = bcrypt.hashSync(this.password, saltRounds);
    };
    next();
});

//Compara que la password ingresada se corresponde con la password encriptada guardada
usuarioSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.password);
};

usuarioSchema.methods.reservar = function(biciId, desde, hasta, cb){
    var reserva = new Reserva({ usuario: this._id,
                                bicicleta: biciId,
                                desde: desde,
                                hasta: hasta});
    console.log(reserva);
    reserva.save(cb);
};

usuarioSchema.methods.enviar_email_bienvenida = function(callback){
    const token = new Token({_userId: this.id, token: crypto.randomBytes(16).toString('hex')}); //creo el token en memoria
    const email_destination = this.email;
    //para persistir el token
    token.save(function(err){
        if (err){
            return console.log(err.message);
        };

        //configuro las opciones del mail
        const mailOptions = {
            from: process.env.mailer_email,
            to: email_destination,
            subject: 'Verificación de cuenta',
            text: 'Hola, \n\n' + 'Por favor para verificar tu cuenta hace click en el siguiente link: \n\n' + process.env.mailer_url + '\/token/confirmation\/' + token.token + '\n\n',
            html: `<p><b>Hola</b> nuevo usuario!<br> Por favor para verficar tu cuenta haga <a href="${process.env.mailer_url}token/confirmation/${token.token}">click aquí</a> </p>` 
        };

        mailer.sendMail(mailOptions, function(err){
            if (err){
                return console.log(err.message);
            }
            console.log('Un mail de verificación fue enviado a  ' + email_destination + '.');
        });
    });
};

usuarioSchema.methods.resetPassword = function(callback) {
    const token = new Token ({_userId: this.id, token: crypto.randomBytes(16).toString('hex')});
    const email_destination = this.email;
    token.save(function(err){
        if (err){ return callback(err) };
        const mailOptions = {
            from: 'no-reply@redbicicletas.com',
            to: email_destination,
            subject: 'Reseteo de password de cuenta',
            text: 'Hola,\n\n' + 'Para resetear el password de tu cuenta por favor hace click en el siguiente link: \n' + 'http://localhost:3000' + '\/resetPassword\/' + token.token + '\n',
            html: `<p><b>Hola!</b><br> Para resetear el password de tu cuenta por favor hace <a href="http://localhost:3000/resetPassword/${token.token}">click aquí</a> </p>` 
        };
        mailer.sendMail(mailOptions, function(err){
            if (err) return callback(err);
            console.log('Se envió un email para resetear el password a: ' + email_destination + '.');
        });
        callback(null);
    });
};

//condition = condición de búsqueda o de creación
usuarioSchema.statics.findOneOrCreateByGoogle = function findOneOrCreate(condition, callback){
    //para guardar la referencia del usuario
    const self = this;
    console.log(condition);
    self.findOne({
        //esto ya es mongoose -- le pasamos el objeto de búsqueda OR por el googleId (id del profile) o el email
        $or:[
            {'googleId': condition.id}, {'email': condition.emails[0].value}
        ]
    }, (err, result) => {
        if (result) {
            //el usuario ya existe
            callback(err, result)
        } else {
            //el usuario no existe - hay que crearlo
            console.log('----- CONDITION -----');
            console.log(condition);
            let values = {};
            values.googleId = condition.id;
            values.email = condition.emails[0].value;
            values.nombre = condition.displayName || 'SIN NOMBRE';
            values.verificado = true;
            values.password = crypto.createHash('sha256').update(condition.id).digest('hex');
            console.log('----- VALUES -----');
            console.log(values);
            self.create(values, (err, result) => {
                if (err){ console.log(err) };
                return callback(err, result);
            })
        }
    })
};

usuarioSchema.statics.findOneOrCreateByFacebook = function findOneOrCreate(condition, callback){
    //para guardar la referencia del usuario
    const self = this;
    console.log(condition);
    self.findOne({
        //esto ya es mongoose -- le pasamos el objeto de búsqueda OR por el googleId (id del profile) o el email
        $or:[
            {'facebookId': condition.id}, {'email': condition.emails[0].value}
        ]
    }, (err, result) => {
        if (result) {
            //el usuario ya existe
            callback(err, result)
        } else {
            //el usuario no existe - hay que crearlo
            console.log('----- CONDITION -----');
            console.log(condition);
            let values = {};
            values.facebookId = condition.id;
            values.email = condition.emails[0].value;
            values.nombre = condition.displayName || 'SIN NOMBRE';
            values.verificado = true;
            values.password = crypto.createHash('sha256').update(condition.id).digest('hex');
            console.log('----- VALUES -----');
            console.log(values);
            self.create(values, (err, result) => {
                if (err){ console.log(err) };
                return callback(err, result);
            })
        }
    })
};

module.exports = mongoose.model('Usuario', usuarioSchema);