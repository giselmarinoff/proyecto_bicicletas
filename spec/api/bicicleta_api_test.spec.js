var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');
var request = require('request');
var server = require('../../bin/www'); //trae el server

var base_url = "http://localhost:3000/api/bicicletas";

describe('Testing API Bicicletas', function() {

    beforeAll((done) => { mongoose.connection.close(done) });
    
    beforeAll(function(done){
        var mongoDBTest = 'mongodb://localhost/test';
        const test = mongoose.connect(mongoDBTest, {useNewUrlParser: true, useUnifiedTopology: true});
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'mongoDB connection error'));
        db.once('open', function(){
            console.log('We are connected to test database');
        });
        Bicicleta.deleteMany({ }, function(err, success) {
            if (err) {console.log(err)};
            done();
        });
    });

    afterEach(function(done) {
        Bicicleta.deleteMany({ }, function(err, success) {
            if (err) {console.log(err)};
            done();
        });
    });

    describe('GET BICICLETAS /', function() {
        it('Status 200', function(done) {
            request.get(base_url, function(error, response, body){
                var result = JSON.parse(body);
                expect(response.statusCode).toBe(200);
                expect(result.bicicletas.length).toBe(0);
                done();
            });    
        });
    });

    describe('GET BICICLETAS /:id', function(){
        it('Status 200', function(done) {
            Bicicleta.todas(function(err, bicis){
                expect(bicis.length).toBe(0);
            });
            var bici1 = new Bicicleta({code: 1, color: 'verde', modelo: 'montaña', ubicacion: [47.8477392, 11.1614675]});
            Bicicleta.add( bici1, function(err, newBici){
                if (err) {console.log(err)};
                var bici2 = new Bicicleta({code: 2, color: 'marron', modelo: 'urbana', ubicacion: [47.8477392, 11.1614675]});
                Bicicleta.add( bici2, function(err, newBici){
                    if (err) {console.log(err)};
                    Bicicleta.todas(function(err, bicis){
                        expect(bicis.length).toBe(2);
                        expect(bicis[0].code).toBe(bici1.code);
                        expect(bicis[1].code).toBe(bici2.code);
                    });
                });
            });
            request.get(base_url + '/' + bici1.code, function(error, response, body){
                var result = JSON.parse(body);
                expect(response.statusCode).toBe(200);
                expect(result.code).toBe(bici1.code);
                expect(result.color).toBe(bici1.color);
                expect(result.modelo).toBe(bici1.modelo);
                done();
            });
        });

        it('Status 400', function(done) {
            Bicicleta.todas(function(err, bicis){
                expect(bicis.length).toBe(0);
            });
            var bici1 = new Bicicleta({code: 1, color: 'verde', modelo: 'montaña', ubicacion: [47.8477392, 11.1614675]});
            Bicicleta.add( bici1, function(err, newBici){
                if (err) {console.log(err)};
                Bicicleta.todas(function(err, bicis){
                    expect(bicis.length).toBe(1);
                    expect(bicis[0].code).toBe(bici1.code);
                });
            });
            request.get(base_url + '/' + '2', function(error, response, body){
                var result = JSON.parse(body);
                expect(response.statusCode).toBe(400);
                expect(result.code_error).toBe('Not found');
                done();
            });
        });
    });

    describe('POST BICICLETAS /create', function(){
        it('Status 200', function(done) {
            var headers = {'Content-Type': 'application/json'};
            var bici = '{ "code": "10", "color": "violeta", "modelo": "montaña", "lat":"47.8477392", "long": "11.1614675"}';
            request.post({
                headers: headers,
                url: base_url + '/create',
                body: bici
                }, 
                function(error, response, body){
                    var result = JSON.parse(body);
                    expect(response.statusCode).toBe(200);
                    expect(result.code).toBe(10);
                    expect(result.color).toBe('violeta');
                    Bicicleta.findByCode(10, function(err, bici){
                        expect(bici.color).toBe('violeta');
                        expect(bici.modelo).toBe('montaña');
                    })
                    done();
                }
            );
        })
    });
});

/* TEST TRABAJANDO CON MODELO SIN PERSISTENCIA
beforeEach(function(){
    Bicicleta.todas = [];
});

describe('Bicicleta API', function(){

    describe('GET BICICLETAS /', function(){
        it('Status 200', function() {
            expect(Bicicleta.todas.length).toBe(0);
            var bici1 = new Bicicleta(1,'verde','montaña', [47.841830,11.1444880]);
            Bicicleta.add(bici1);
            request.get('http://localhost:3000/api/bicicletas', function(error, response, body){
                expect(response.statusCode).toBe(200);
            })
        })
    });

    describe('GET BICICLETAS /:id', function(){
        it('Status 200', function(done) {
            expect(Bicicleta.todas.length).toBe(0);
            var bici = new Bicicleta(10, 'verde', 'montaña', [47.8477392, 11.1614675]);
            Bicicleta.add(bici);
            expect(Bicicleta.findById(10).color).toBe("verde");
            expect(Bicicleta.todas.length).toBe(1);
            request.get('http://localhost:3000/api/bicicletas/10', function(error, response, body){
                expect(response.statusCode).toBe(200);
                done();
            });
        });
    });
    
    describe('POST BICICLETAS /create', function(){
        it('Status 200', function(done) {
            var headers = {'Content-Type': 'application/json'};
            var bici = '{ "id": "10", "color": "violeta", "modelo": "montaña", "lat":"47.8477392", "long": "11.1614675"}';
            request.post({
                headers: headers,
                url: 'http://localhost:3000/api/bicicletas/create',
                body: bici
                }, 
                function(error, response, body){
                    expect(response.statusCode).toBe(200);
                    expect(Bicicleta.findById(10).color).toBe("violeta");
                    done(); //hasta que no se ejecute done el test no termina
                }
            );
        })
    });

    describe('POST BICICLETAS /update', function(){
        it('Status 200', function(done) {
            expect(Bicicleta.todas.length).toBe(0);
            var bici = new Bicicleta(10, 'violeta', 'montaña', [47.8477392, 11.1614675]);
            Bicicleta.add(bici);
            expect(Bicicleta.todas.length).toBe(1);
            expect(Bicicleta.findById(10).color).toBe("violeta");
        
            var headers = {'Content-Type': 'application/json'};
            var bicinew = '{ "id": "10", "color": "rojo", "modelo": "urbana", "lat":"47.8477392", "long": "11.1614675"}';
            request.post({
                headers: headers,
                url: 'http://localhost:3000/api/bicicletas/update',
                body: bicinew
                }, 
                function(error, response, body){
                    expect(response.statusCode).toBe(200);
                    expect(Bicicleta.findById(10).color).toBe("rojo");
                    done(); //hasta que no se ejecute done el test no termina
                }
            );
        });
    });

    describe('POST BICICLETAS /delete', function(){
        it('Status 204', function(done) {
            expect(Bicicleta.todas.length).toBe(0);
            var bici = new Bicicleta(10, 'violeta', 'montaña', [47.8477392, 11.1614675]);
            Bicicleta.add(bici);
            expect(Bicicleta.findById(10).color).toBe("violeta");
            expect(Bicicleta.todas.length).toBe(1);

            var headers = {'Content-Type': 'application/json'};
            var req = '{"id": "10"}'
            request.post({
                headers: headers,
                url: 'http://localhost:3000/api/bicicletas/delete',
                body: req
                }, 
                function(error, response, body){
                    expect(response.statusCode).toBe(204);
                    expect(Bicicleta.todas.length).toBe(0);
                    done(); //hasta que no se ejecute done el test no termina
                }
            );
        });
    });

});

*/