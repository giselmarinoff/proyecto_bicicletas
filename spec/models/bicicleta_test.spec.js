var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');

describe('Testing Modelo de Bicicletas', function() {

    beforeAll((done) => { mongoose.connection.close(done) });

    beforeAll(function(done){
        var mongoDB = 'mongodb://localhost/test';
        mongoose.connect(mongoDB, {useNewUrlParser: true, useUnifiedTopology: true});
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'mongoDB connection error'));
        db.once('open', function(){
            console.log('We are connected to test database');
        });
        Bicicleta.deleteMany({ }, function(err, success) {
            if (err) {console.log(err)};
            done();
        });
    });

    afterEach(function(done) {
        Bicicleta.deleteMany({ }, function(err, success) {
            if (err) {console.log(err)};
            done();
        });
    });
    
    describe('Bicicleta.createInstance', function() {
        it('Crea una instancia de bicicleta', function() {
            var bici = Bicicleta.createInstance(1,'verde','montaña', [47.841830,11.1444880]);
            expect(bici.code).toBe(1);
            expect(bici.color).toBe('verde');
            expect(bici.modelo).toBe('montaña');
            expect(bici.ubicacion[0]).toEqual(47.841830);
            expect(bici.ubicacion[1]).toEqual(11.1444880);
        })
    });

    describe('Bicicleta.todas', function() {
        it('Comienza vacio', function(done) {
            Bicicleta.todas(function(err, bicis){
                expect(bicis.length).toBe(0);
                done();
            });
        });
    });

    describe('Bicicleta.add', function() {
        it('Agrega una bici', function(done) {
            var bici = new Bicicleta({code: 1, color: 'verde', modelo: 'montaña'})
            Bicicleta.add( bici, function(err, newBici){
                if (err) {console.log(err)};
                Bicicleta.todas(function(err, bicis){
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toBe(bici.code);
                    done();
                });
            });
        });
    });

    describe('Bicicleta.findByCode', function() {
        it('Devuelve una bici', function(done) {
            Bicicleta.todas(function(err, bicis){
                expect(bicis.length).toBe(0);
                var bici1 = new Bicicleta({code: 1, color: 'verde', modelo: 'montaña'});
                Bicicleta.add(bici1, function(err, newBici){
                    if (err) {console.log(err)};
                    var bici2 = new Bicicleta({code: 2, color: 'rojo', modelo: 'deportiva'});
                    Bicicleta.add(bici2, function(err, newBici){
                        if (err) {console.log(err)};
                        Bicicleta.todas(function(err, bicis){
                            expect(bicis.length).toBe(2);
                        });
                        Bicicleta.findByCode(1, function(err, itemBici){
                            expect(itemBici.code).toBe(bici1.code);
                            expect(itemBici.color).toBe(bici1.color);
                            expect(itemBici.modelo).toBe(bici1.modelo);
                            done();
                        });
                    });
                });
            });
        });
    });

    describe('Bicicleta.removeByCode', function() {
        it('Elimina una bici', function(done) {
            Bicicleta.todas(function(err, bicis){
                expect(bicis.length).toBe(0);
                var bici1 = new Bicicleta({code: 1, color: 'verde', modelo: 'montaña'});
                Bicicleta.add(bici1, function(err, newBici){
                    if (err) {console.log(err)};
                    var bici2 = new Bicicleta({code: 2, color: 'rojo', modelo: 'deportiva'});
                    Bicicleta.add(bici2, function(err, newBici){
                        if (err) {console.log(err)};
                        Bicicleta.todas(function(err, bicis){
                            expect(bicis.length).toBe(2);
                        });
                        Bicicleta.removeByCode(1, function(err, success){
                            if (err) {console.log(err)};
                            Bicicleta.todas(function(err, bicis){
                                expect(bicis.length).toBe(1);
                                expect(bicis[0].code).toBe(bici2.code);
                                expect(bicis[0].color).toBe(bici2.color);
                                expect(bicis[0].modelo).toBe(bici2.modelo);
                                done();
                            });
                        });
                    });
                });
            });
        });
    });
});

/* TEST TRABAJANDO CON MODELO SIN PERSISTENCIA

beforeEach(function(){
    Bicicleta.todas = [];
});

//Test del método bicicleta.todas
describe('bicicleta.todas', function() {
    it('comienza vacio', function() {
        //expect(propiedad o valor a testear).toBe(valoresperado)
        expect(Bicicleta.todas.length).toBe(0);
    })
});

//Test del método bicicleta.add
describe('bicicleta.add', function() {
    it('agrega ítem', function() {
        //Compruebo que sea cero por si otro test lo modificó --precondición
        expect(Bicicleta.todas.length).toBe(0); 
        //Creo una bici nueva y la agrego
        var bici1 = new Bicicleta(1,'verde','montaña', [47.841830,11.1444880]);
        Bicicleta.add(bici1);
        //Compruebo que se haya agregado correctamente
        expect(Bicicleta.todas.length).toBe(1);
        expect(Bicicleta.todas[0]).toBe(bici1);
    })
});

//Test del método bicicleta.findById
describe('bicicleta.findById', function() {
    it('encuentra ítem 1', function() {
        expect(Bicicleta.todas.length).toBe(0); 
        
        var bici1 = new Bicicleta(1,'verde','montaña', [47.841830,11.1444880]);
        var bici2 = new Bicicleta(2,'rojo','deportiva', [47.8471771,11.1510966]);
        Bicicleta.add(bici1);
        Bicicleta.add(bici2);
        
        var targetBici = Bicicleta.findById(1);
        
        expect(targetBici.id).toBe(1);
        expect(targetBici.color).toBe(bici1.color);
        expect(targetBici.modelo).toBe(bici1.modelo);
    })
});

//Test del método bicicleta.removeById
describe('bicicleta.removeById', function() {
    it('elimina el ítem 1', function() {
        expect(Bicicleta.todas.length).toBe(0); 
        
        var bici1 = new Bicicleta(1,'verde','montaña', [47.841830,11.1444880]);
        var bici2 = new Bicicleta(2,'rojo','deportiva', [47.8471771,11.1510966]);
        Bicicleta.add(bici1);
        Bicicleta.add(bici2);

        expect(Bicicleta.todas[0]).toBe(bici1);
        expect(Bicicleta.todas[1]).toBe(bici2);
        
        Bicicleta.removeById(1);
        expect(Bicicleta.todas[0]).toBe(bici2);
        expect(Bicicleta.todas.length).toBe(1);

        Bicicleta.removeById(2);
        expect(Bicicleta.todas.length).toBe(0);
    })
});

*/