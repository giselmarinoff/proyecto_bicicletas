En este repositorio se encuentran todos los contenidos aplicados del curso "Desarrollo del lado servidor: NodeJS, Express y MongoDB"
de la Universidad Austral, dictado por Ezequiel Lamónica.

En dicho proyecto iniciando en localhost:3000/ directamente van a poder encontrar la página principal, con el
template especificado en el tutorial con las modificaciones requeridas por el proyecto.

Iniciando en localhost:3000/welcome van a poder observar el mensaje "Welcome to Express" conservado
del index original de cuando se inició el proyecto de express. Este index fue reubicado siguiendo 
la lógica requerida del proyecto.

Iniciando en localhost:3000/bicicletas van a poder observar la lista de bicicletas en memoria, y van a
poder realizar el ABM de las mismas.