var express = require('express');
var router = express.Router();
var controllerAPI = require('../../controllers/api/usuarioControllerAPI');

router.get('/', controllerAPI.usuario_list);
router.post('/create', controllerAPI.usuario_create);
router.post('/reservar', controllerAPI.usuario_reservar);

module.exports = router;