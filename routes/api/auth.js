var express = require('express');
const passport = require('passport');
var router = express.Router();
var authController = require('../../controllers/api/authControllerAPI');

router.post('/authenticate', authController.authenticate);
router.post('/forgotPassword', authController.forgotPassword);
router.post('/facebook_token', passport.authenticate('facebook-token'), authController.authFacebookToken);
// passport.authenticate('facebook-token') nos permite usar el req.user en el controller de authFacebookToken

module.exports = router;