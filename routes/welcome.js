var express = require('express');
var router = express.Router();
var welcomeController = require('../controllers/welcome');

router.get('/', welcomeController.welcome_msg);

module.exports = router;
