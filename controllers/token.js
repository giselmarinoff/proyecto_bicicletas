var Usuario = require('../models/usuario');
var Token = require('../models/token');

//callback de la confirmacón del token
exports.confirmationGet = function(req, res, next){
    Token.findOne({ token: req.params.token }, function(err, token){
        if (!token) {
            return res.status(400).send({ type: 'not-verified', msg: 'No se ha encontrado un usuario con este token. Quizas haya expirado. Por favor solicita uno nuevo.'});
        };
        Usuario.findById(token._userId, function(err, usuario){
            if (!usuario) {
                return res.status(400).send({ msg: 'No se ha encontrado un usuario con este token.'});
            };
            if (usuario.verificado) {
                return res.redirect('/usuarios'); //redirect a la lista de usuarios
            };
            usuario.verificado = true;
            usuario.save(function(err){
                if (err) { return res.status(500).send({ msg: err.message}); }
                res.redirect('/');
            });
        });
    });
};
