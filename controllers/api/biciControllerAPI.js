var bicicleta = require('../../models/bicicleta');

exports.bicicleta_list = function(req, res){
    bicicleta.todas(function(err, bicis){
        res.status(200).json({
            bicicletas: bicis
        });
    })  
};

exports.bicicleta_create = function(req, res){
    var bici = new bicicleta({code: req.body.code, color: req.body.color, modelo: req.body.modelo})
    bici.ubicacion = [req.body.lat, req.body.long];
    bicicleta.add(bici, function(err){
        res.status(200).json(bici);
    });
};
 
exports.bicicleta_delete = function(req, res){
  bicicleta.removeByCode(req.body.code, function(err){
    res.status(204).send(); //se usa el 204 porque no se devuelve nada
  });
}

exports.bicicleta_item = function(req, res){
    bicicleta.findByCode(req.params.code, function(err, bici){
        if (bici === null){
            var error = {   code_error: 'Not found',
                            message: 'Bicicleta no encontrada'}
            res.status(400).json(error);
        } else {
        res.status(200).json(bici);
        };
    });
};

exports.bicicleta_update = function(req, res){
    var update = {  code: req.body.code,
                    color: req.body.color,
                    modelo: req.body.modelo,
                    ubicacion: [req.body.lat, req.body.long] };
    bicicleta.updateByCode(req.body.code, update, function(err, bici){
        if (bici.n > 0) {
            console.log(bici);
            res.status(200).json(update);
        }
        else{
            var error = {   code_error: 'Not found',
                            message: 'Bicicleta no encontrada'}
            res.status(400).json(error);
        }
    });
};

/* TRABAJANDO CON MODELO SIN PERSISTENCIA

exports.bicicleta_list = function(req, res){
    res.status(200).json({
        bicicletas: bicicleta.todas
    });
}

exports.bicicleta_create = function(req, res){
    var bici = new bicicleta(req.body.id, req.body.color, req.body.modelo);
    bici.ubicacion = [req.body.lat, req.body.long];
    bicicleta.add(bici);
    res.status(200).json({
        bicicletas: bicicleta.todas
        // o tambien puede devolver solamente la nueva bicicleta añadida:
        // bicicleta: bici
    });
}

exports.bicicleta_delete = function(req, res){
  bicicleta.removeById(req.body.id);
  res.status(204).send();
  //se usa el 204 porque no se devuelve nada
}

exports.bicicleta_update = function(req, res){
    var bici = bicicleta.findById(req.body.id);
    bici.id = req.body.id;
    bici.modelo = req.body.modelo;
    bici.color = req.body.color;
    bici.ubicacion = [req.body.lat, req.body.long];
    res.status(200).json({
        bicicletas: bicicleta.todas
        // o tambien puede devolver solamente la nueva bicicleta añadida:
        // bicicleta: bici
        //Falta definir el error cuando no se encuentra el id de bici
    });
}

exports.bicicleta_item = function(req, res){
    var bici = bicicleta.findById(req.params.id);
    res.status(200).json({bici});
}

*/