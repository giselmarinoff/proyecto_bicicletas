var Usuario = require('../../models/usuario');

exports.usuario_list = function (req, res){
    Usuario.find({}, function(err, usuarios){
        res.status(200).json({
            usuarios: usuarios
        });
    });
};

exports.usuario_create = function(req, res){
    var user = new Usuario({nombre: req.body.nombre, email: req.body.email, password: req.body.password});
    user.save(function(err){
        res.status(200).json(user);
    });
};

exports.usuario_reservar = function(req, res){
    Usuario.findById(req.body.userId, function(err, user){
        console.log(user);
        user.reservar(req.body.biciId, req.body.desde, req.body.hasta, function(err, reserva){
            console.log('Reserva realizada!');
            res.status(200).json(reserva);
        });
    });
};