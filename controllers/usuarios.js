var usuario = require('../models/usuario');

//controlador del index
exports.list = function(req, res, next){
    usuario.find({}, function(err, usuarios){
        res.render('usuarios/index', {usuarios: usuarios});  
    });
};

//controlador de create (ver la página para crear)
exports.create_get = function(req, res, next){
    res.render('usuarios/create', {errors:{}, usuario: new usuario()});
};

//controlador de create (para cuando se agrega el usuario)
exports.create_post = function(req, res, next){
    if (req.body.password != req.body.confirm_password){
        res.render('usuarios/create', { errors: {confirm_password: {message: 'Las contraseñas no coinciden'}}, 
                                        usuario: new usuario({nombre: req.body.nombre, email: req.body.email})});
        return;
    }    
    usuario.create({nombre: req.body.nombre, email: req.body.email, password: req.body.password}, function(err, nuevoUsuario){
        if (err){
            res.render('usuarios/create', {errors: err.errors, usuario: new usuario({nombre: req.body.nombre, email: req.body.email})});
        } else {
            nuevoUsuario.enviar_email_bienvenida();
            res.redirect('/usuarios');
        };
    });
};

//controlador de update (ver el formulario)
exports.update_get = function(req, res, next){
    usuario.findById(req.params.id, function(err, usuario){
        res.render('usuarios/update', {errors:{}, usuario: usuario});
    });
};

//controlador de update (para enviar los datos modificados)
exports.update_post = function(req, res, next){
    var update_values = {nombre: req.body.nombre, email: req.body.email};
    usuario.findByIdAndUpdate(req.params.id, update_values, function(err, usuario){
        if (err){
            console.log(err);
            res.render('usuarios/update', {errors: err.errors, usuario: new Usuario({nombre: req.body.nombre, email: req.body.email})});
        } else {
            res.redirect('/usuarios');
            return;
        }
    });
};

//para eliminar un usuario
exports.delete = function(req, res, next){
    usuario.findByIdAndDelete(req.body.id, function(err){
        if (err){
            next(err);
        } else {
            res.redirect('/usuarios');
        };
    });
};

//Para ver un usuario
exports.usuario_item = function(req, res, next){
    usuario.findById(req.params.id, function(err, usuario){
        res.render('usuarios/item', {errors:{}, usuario: usuario});      
    });
};