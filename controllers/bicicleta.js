var bicicleta = require('../models/bicicleta');

//controlador del index
exports.bicicleta_list = function(req, res, next){
    bicicleta.todas(function(err, bicis){
        res.render('bicicletas/index', {bicis})
    });
};

//controlador de create (ver la página para crear)
exports.bicicleta_create_get = function(req, res, next){
    res.render('bicicletas/create', {errors:{}, usuario: new bicicleta()});
};

//controlador de create (para cuando se agrega la bicicleta)
exports.bicicleta_create_post = function(req, res, next){
    var ubicacion = [req.body.lat, req.body.long];
    bicicleta.create({code: req.body.code, color: req.body.color, modelo: req.body.modelo, ubicacion: ubicacion}, function(err, nuevaBici){
        if (err){
            res.render('bicicletas/create', {errors: err.errors, 
                                            bici: new bicicleta({code: req.body.code,
                                                                color: req.body.color,
                                                                modelo: req.body.modelo,
                                                                ubicacion: ubicacion})});
        } else {
            res.redirect('/bicicletas');
        };
    });
}

//controlador de update (ver el formulario)
exports.bicicleta_update_get = function(req, res, next){
    bicicleta.findByCode(req.params.code, function(err, bici){
        res.render('bicicletas/update', {bici});
    });
};

//controlador de update (para enviar los datos modificados)
exports.bicicleta_update_post = function(req, res, next){
    var update = { code: req.body.code,
        color: req.body.color,
        modelo: req.body.modelo,
        ubicacion: [req.body.lat, req.body.long] };
    bicicleta.updateByCode(req.params.code, update, function(err, bici){
        if (err){
            res.render('bicicletas/update', {errors: err.errors, 
                                            bici: new bicicleta({code: req.body.code,
                                                                color: req.body.color,
                                                                modelo: req.body.modelo,
                                                                ubicacion: ubicacion})});
        } else {
            res.redirect('/bicicletas');
        };
    });
};

//controlador de remove (para cuando se quiere eliminar una bicicleta)
exports.bicicleta_delete_post = function(req, res, next){
    bicicleta.removeByCode(req.body.code, function(err){
        res.redirect('/bicicletas');
    });
};

//controlador de item (ver una bicicleta)
exports.bicicleta_item = function(req, res, next){
    bicicleta.findByCode(req.params.code, function(err, bici){
        res.render('bicicletas/item', {bici});
    });
}